# Agile

| Role | Name | Purpose |
| -- | -- | -- |
| Manager |  | Watch the time. Ensure everyone has a voice. Keep team focused. Help to answer questions. |
| Recorder |  | Document team's answers. Help to answer questions. |
| Spokesperson | | Speak for the team. Help to answer questions. |

## Model 1

*   What is Agile? - 11m video by Mark Sheed - [https://youtu.be/Z9QbYZh1YXY](https://youtu.be/Z9QbYZh1YXY)
*   Agile Manifesto
    *   values
        *   [https://agilemanifesto.org/](https://agilemanifesto.org/)
        *   With explanation: [https://www.agilealliance.org/agile101/the-agile-manifesto/](https://www.agilealliance.org/agile101/the-agile-manifesto/)
    *   principles
        *   [https://agilemanifesto.org/principles.html](https://agilemanifesto.org/principles.html)
        *   With explanation: [https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/](https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/)

## Questions

1. Is agile a process?

2. Is agile a framework?

3. What is agile?

4. Copy the 12 principles of agile development and past them here. For each principle, come up with a one word summary/title for each.

5. Copy and paste the 4 values of agile. For each, explain its significance or importance.

<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;

# Agile vs Waterfall

| Role | Name | Purpose |
| -- | -- | -- |
| Manager |  | Watch the time. Ensure everyone has a voice. Keep team focused. Help to answer questions. |
| Recorder |  | Document team's answers. Help to answer questions. |
| Spokesperson | | Speak for the team. Help to answer questions. |

## Model 1

Project success rates by process methodology from Ambysoft's 2013 Project Success Rates Survey http://www.ambysoft.com/surveys/success2013.html .

```mermaid
pie
    title Projects using an Agile Process
    "Success" : 64
    "Challenged" : 28
    "Failed" : 8
```

```mermaid
pie
    title Projects using Waterfall
    "Success" : 49
    "Challenged" : 33
    "Failed" : 18
```

Use model 1 to answer the questions below.

1. Which process methodology had the larger success rate?

2. Which process methodology had the larger fail rate?

3. Which process methodology has the larger challenged rate?

4. What do you think is meant by "challenged" as opposed to failed?



## Model 2

Project success rates by process methodology from the Standish Group's 2015 CHAOS Report https://www.infoq.com/articles/standish-chaos-2015/ .


```mermaid
pie
    title Projects using an Agile Process
    "Success" : 42
    "Challenged" : 49
    "Failed" : 9
```

```mermaid
pie
    title Projects using Waterfall
    "Success" : 14
    "Challenged" : 57
    "Failed" : 29
```

Use model 2 to answer the questions below.

1. Which process methodology had the larger success rate?

2. Which process methodology had the larger fail rate?

3. Which process methodology has the larger challenged rate?

4. Does the results from the 2015 CHAOS Report (model 2) confirm or reject those from Ambysoft's 2013 Project Success Rates Survey (model 1)?

5. If you were planning to start a new project, which process methodology would you likely want to learn more about and possibly use?


## Model 3

See Sweeney's article "Agile vs Waterfall: Which Method is More Successful" https://clearcode.cc/blog/agile-vs-waterfall-method/

Use the above article to answer the following questions.

1. In section 1, what phases/stages does waterfall have that agile does not?

2. Explain the "Boat" diagram.

3. The Mona Lisa is often used to illustrate incremental and iterative development. Search for these and summarize your understanding of these concepts and how they relate to agile development.


## Other articles on Waterfall vs Agile

*   [https://clearcode.cc/blog/agile-vs-waterfall-method/](https://clearcode.cc/blog/agile-vs-waterfall-method/)
*   [https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/](https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/)
*   [https://www.infoq.com/articles/standish-chaos-2015](https://www.infoq.com/articles/standish-chaos-2015)
a

<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;

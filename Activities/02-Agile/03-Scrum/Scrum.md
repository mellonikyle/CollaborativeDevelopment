# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Team Roles


_Record role assignments here._


| Role | Name | Purpose |
| -- | -- | -- |
| Manager |  | Watch the time. Ensure everyone has a voice. Keep team focused. Help to answer questions. |
| Recorder |  | Document team's answers. Help to answer questions. |
| Spokesperson | | Speak for the team. Help to answer questions. |


# Model 0: What is Scrum? Video

What is Scrum? - 1:20m video by Scrum Alliance - [https://youtu.be/TRcReyRYIMg](https://youtu.be/TRcReyRYIMg)


# Model 1: Scrum Roles

Find the answers to the questions below in [Scrum: A Breathtakingly Brief and Agile Introduction](https://www.agilelearninglabs.com/resources/scrum-introduction/).


## Questions (10 min)


1. How many roles does Scrum recognize?
2. List the Scrum roles.
3. Which Scrum role is responsible for each of the following?
    - a. Deciding which tools and techniques that will be used to get the work done.
    - b. Owning and ordering the product backlog.
    - c. Solving any problems that are keeping the team from getting its work done.
    - d. Deciding whether to accept work from the team as meeting the criteria.
    - e. Completing work from the product backlog.
    - f. Deciding who will do what work.
    - g. Helping the team improve its performance.
    - h. Estimating how long it will take to complete a user story.
    - i. Representing the interests of the business and the customers.


# Model 2: Scrum Artifacts

Find the answers to the questions below in [Scrum: A Breathtakingly Brief and Agile Introduction](https://www.agilelearninglabs.com/resources/scrum-introduction/).


## Questions (15 min)


1. How many tools does Scrum use?
2. List the Scrum artifacts.
3. Which Scrum artifact is each of the following characteristic of?
    - a. Contains both stories and tasks.
    - b. Shows the status of individual tasks.
    - c. Serves as an agreement between team members about what it means for a task to be completed.
    - d. Has a limited life-span.
    - e. Contains all user stories for the product.
    - f. Shows how much work has been done over time.
    - g. Is ordered with most important story at the top.
4. What purpose do all of the Scrum artifacts have in common?
5. How should the stories at the top of the Product Backlog differ from the stories near the bottom? Why is that the case?
6. What is the relationship between stories and tasks?
7. List the information that should be included in every story.
8. Explain how the Task Board should be used.


# Model 3: The Sprint Cycle

Find the answers to the questions below in [Scrum: A Breathtakingly Brief and Agile Introduction](https://www.agilelearninglabs.com/resources/scrum-introduction/).


## Questions (15 min)

1. How many different types of meeting are part of the Sprint Cycle?
2. List these meetings.
3. Which meeting is each of the following characteristic of?
    - a. Happens at the beginning of the sprint.
    - b. Consists of two parts - "What will we do?" and "How will we do it?"
    - c. Results in the sprint backlog.
    - d. Consists of team members answering three questions.
    - e. Is used for making the entire team aware of team members' status and issues.
    - f. Is when the team "grooms" the product backlog by making stories clearer and more defined.
    - g. Is when the team demonstrates completed work to the stakeholders.
    - h. Is when the team discusses ways to improve the team's performance for future sprints.
4. A sprint lasts until all the work is finished - true or false? Explain your answer.
5. What are the advantages of a shorter sprint cycle?


## More Information on Scrum

- Another more detailed description of Scrum: https://youtu.be/RaaBrPCo_Mw


## Related Topics

- Kanban: https://www.atlassian.com/agile/kanban
- Lean: https://www.youtube.com/watch?v=a255lkYgIpI


<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

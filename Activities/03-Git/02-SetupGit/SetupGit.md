# Setup Git

This activity will help you get started with git.


## Objectives

After completing this tutorial you will be able to...

-   Download and install git.
-   Start a terminal for use with git.
-   Use git's online help.
-   Configure git.
-   Create a new project managed by git.


## Team

| Role      | Name | Responsibility |
| --------- | ---- | -------------- |
| Manager   |      | Manages time. Speaks for the team. Helps solve problem. |
| Pilot     |      | **Place in the middle.** Manipulates git on behalf of the team. |
| Recorder  |      | Write answers on behalf of the team. Help solve problems. |


## Helpful resources

-  [Command line cheat sheet](CheatSheet.md)
-   https://git-scm.com/doc
-   https://www.atlassian.com/git/tutorials/


## Download and install Git

Download and install git for your operating system:

-   <https://git-scm.com/downloads>

Help **each of your team members** download and install git for there system.

If you are working in a lab that has git installed, and/or your do not have a
personal device with you, discuss with your team how you to download and
install git for your machine based on the website above. Make sure everyone in
your team is confident that they can install it on their personal machine when
they get home. Note any questions anyone has here and ask them during report
out.


## Start a terminal for use with git

For the remainder of this activity only the team's pilot should carry out these
instructions.

Starting a terminal:

-   **Windows**:
    - Right-click in a folder and select "Git Bash Here"
-   **Mac OSX:** Finder -&gt; Applications -&gt; Utilities -&gt;
    Terminal.app
-   **Linux:** will vary depending on your window manager


## Getting help

1. Run the following commands.

        git help
        git help -ag
        git help init

2.  What does `git help` do?
3.  What does `-ag` cause `git help` to do?


## G. Identify yourself

1. Run the following commands, replacing BOGUS NAME and BOGUS@EMAIL with
your name and email.

        git config --global user.name 'BOGUS NAME'
        git config --global user.email 'BOGUS@EMAIL'

    WARNING: The name and email you give will be listed on each commit you make.
    If this repository is ever published, your name and email on every commit
    will be too. Also, if you are working on a shared computer, you should
    consider changing this configuration before you walk away.

2.  What are these commands doing?
3.  What is the purpose of `--global`?


## H. Create a repository

1. From the command line, run the following commands.

        mkdir first_project
        cd first_project
2.  By default any file that starts with `.` is hidden. How do you display a hidden file?
3. Run this command to show the hidden files in the current directory. Are there any?
4. Now run the following command.

        git init
5. Check for hidden files again.  What was created by `git init`?
6.  What do you think would happen if you delete `.git`?
7.  Using your observations to the previous questions, answer the following.
    You find an old project on your hard drive. You do not remember if
    it is a under version control. What could you look for to determine
    if the project is being managed using git?


## Practice

**One at a time,** help each team member do the following.

1. Open a terminal on their desktop.
2. Configure git with their name and email.
3. Create a new directory, change into it, and initialize git for use in the new directory.


--------------------------
Copyright 2016, Darci Burdge and Stoney Jackson

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by-sa/4.0/> .

# Saving Changes

We will use git to keep track of changes made to the files in your
project.

Content Learning Objectives
---------------------------

*After completing this activity, students should be able to:*

-   Determine whether files are changed or unchanged.

-   Determine whether files are in the git working area or the git
    staging area.

-   Move changed files to the git staging area.

-   Commit files in the git staging area.

-   View the log of commits.


## Process Skill Goals

*During the activity, students should make progress toward:*

-   Leveraging prior knowledge and experience of other students.
    (Teamwork)


## Team Roles


| Role      | Name | Responsibility |
| --------- | ---- | -------------- |
| Manager   |      | Manage time. Ensure everyone has a voice. Help solve problems. |
| Recorder  |      | Write answers on behalf of the team. Help solve problems. |
| Presenter and Reflector |      | Speaks for the team. Helps solve problem. Identify what the team is doing well, and suggest how it might improve |


## Model 1: Files can be in one of three states in Git
---------------------------------------------------

```plantuml
@startuml
[*] -> Working: create
Working -> Staging: git add
Staging -> Committed: git commit
Committed -> Working: modify
@enduml
```

**Working:** Changes you made that will not be committed in the next **`git commit`** command.

**Staging:** Changes that will be committed in the next **`git commit`** command.

**Committed:** Changes that have been committed to the project's history.

### Questions (1 min)

1.  What operation will be used to move a change from the Working
    area to the Staging area?

2.  What operation will be used to move a group of changed files from
    the Staging area to the Committed area?


## Model 2: Before Starting to Work on your Program to add Feature Y.

You are working on your computer. You are about to start making changes
to add Feature Y to the program. You enter the following commands (each command comes right after the prompt `$`) and get the following results:

```bash
$ ls
A.java B.java C.java
$ git log
commit db967767f9ab675dba21e08c5d20627b34cbe133 (HEAD -> master, origin/master, origin/HEAD)
Author: Karl R. Wurst <karl@w-sts.com>
Date:   Fri Jan 18 14:25:44 2019 -0500

    Add Feature X.

$ git status
On branch master
Nothing to commit, working tree clean

$
```

### Questions (8 min)

1.  How many files are there in this program? What are their names?

2.  What was the purpose of the last committed change to the project?

3.  Who made the last set of changes to the program?

4.  When did they finish making those changes?

5.  What does **`git log`** tell you about your program?

6.  For each file in the program, place it in the column in the table below that corresponds to its current state from model 1. Below the table, justify your answer.

    | Committed | Working | Staging |
    | -- | -- | -- |
    |    |    |    |


7.  What does **`git status`** tell you about your program?


## Model 3: Starting to Work on your Program to add Feature Y.

To add Feature Y to your program, you determine that you need to make
changes to the file **B.java**.

```bash
$ edit B.java           # Note: edit means "modify using an editor"
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)

        modified: B.java

no changes added to commit (use "git add" and/or "git commit -a")

$
```

### Questions (8 min)

1.  For each file in the program, place it in the column in the table below that corresponds to its current state from model 1. Below the table, justify your answers.

    | Committed | Working | Staging |
    | -- | -- | -- |
    |    |    |    |


2.  Assuming that you are done with making changes to the file B.java,
    what command would move it into the Staging state? How do you know?


## Model 4: Done with One File, But with More to Do

You want to put **B.java** into a state to indicate that you are done
making changes to it. But you still need to make changes to other files
before you have finished Feature Y.

```bash
$ git add B.java

$ git status
On branch master

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

          modified: B.java

$
```

### Questions (1 min)

1.  For each file in the program, place it in the column in the table below that corresponds to its current state from model 1. Below the table, justify your answers.

    | Committed | Working | Staging |
    | -- | -- | -- |
    |    |    |    |



## Model 5: More Work for Feature Y

To finish adding Feature Y, you realize you need to make a change to the
file A.java, and you need to create a new file D.java.

```bash
$ edit A.java

$ edit D.java

$ git status
On branch master

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

        modified: B.java

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)

        modified: A.java

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        D.java

$
```

### Questions (5 min)

1.  For each file in the program, place it in the column in the table below that corresponds to its current state from model 1. Below the table, justify your answers.

    | Committed | Working | Staging |
    | -- | -- | -- |
    |    |    |    |

2.  You have finished making your changes for Feature Y. You want to
    have the files **A.java, B.java,** and **D.java** all to be in the
    Staging state. What command(s) would you give to make this the case?


## Model 6: Done Working on Feature Y

You have finished making all your changes to implement Feature Y, and
have put all the new and changed files into the Staging area.

```bash
$ git status
On branch master

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

        modified: A.java
        modified: B.java
        new file: D.java

$ git commit -m "Add Feature Y"
[master 63b66cf] Add Feature Y
3 files changed, 7 insertions(+), 5 deletions(-)
Create mode 100644 D.java

$ git status
On branch master
Nothing to commit, working tree clean

$ git log
commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4
Author: Karl R. Wurst <karl@w-sts.com>
Date:   Fri Jan 18 16:02:12 2019 -0500

      Add Feature Y

commit db967767f9ab675dba21e08c5d20627b34cbe133 (HEAD -> master, origin/master, origin/HEAD)
Author: Karl R. Wurst <karl@w-sts.com>
Date: Fri Jan 18 14:25:44 2019 -0500

      Add Feature X.
```

### Questions (9 min)

1.  What is the purpose of the first **`git status`** command?

2.  What does **`git commit`** do?

3.  What is the purpose of **`-m "Add Feature Y"`** in the **`git commit`**
    command?

4.  What is the purpose of the second **`git status`** command?

5.  What does the **`git log`** command demonstrate?

6.  In what order does **`git log`** shows commits?

7.  What is the purpose of the
    `commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4`
    part of the log for? Where else do you see that number?

## Practice

Manager, one at a time, have each of your team members work through the following exercise while the other team members assist. Have your recorder make notes of any insights your team discovers along the way.

This will take on average about 10 minutes per person. Probably longer for the first, and less for the last.

1. Create a new directory to hold a new project.
2. Initialize your new directory for use with git.

Use `git log` and `git status` before and after each of the following to confirm your understanding of what is going on.

3. Create a file, stage it, and commit it.
4. Create another file, stage it, and commit it.
5. Modify both files, stage ONE of them, and commit it.
6. Stage the other file and commit it.
7. Delete a file, stage the change, and commit it.
8. Rename a file, stage the change, and commit it.

---
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.

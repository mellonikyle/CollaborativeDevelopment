# Create, Clone, and Push to a GitLab Project

## Learning Objectives

Able to

1. Create an initialized GitLab project

## Team Roles

| Role      | Name | Responsibility |
| --------- | ---- | -------------- |
| Manager and Reporter   |      | Manage time. Ensure everyone has a voice. Help solve problems. |
| Pilot     |      | **Place in middle.** Manipulates GitLab on behalf of the team.
| Recorder  |      | Write answers on behalf of the team. Help solve problems. |


## Model - Creating an Initialized Repository

- GitLab Instructions https://docs.gitlab.com/ee/gitlab-basics/create-project.html


### Questions

1. Your instructor will give you a group, record it here.


2. Write the initials of your team members mashed together. This will be your project name.


3. Based on the instructions above, what does it mean for a repository to be initialized with a README?


4. Work through the instructions above to create a new repository initialized with a README. Record any insights or questions your team may have while working through the instructions.



## Model - Cloning a Repository

![clone url](./images/clone-url.png)


### Questions

1. Use the blue "Clone" button to reveal the clone URLs for your project.

2. Copy the clone URL for HTTPS.

3. Open a git-bash on your desktop, so that your current working directory is your desktop.

4. Clone your project as follows (right click on the title bar of the git-bash window to paste)

    ```bash
    $ git clone PASTE-CLONE-URL-HERE
    ```
5. What directory was created?

6. Change into that directory. And list its files.

7. Edit README.md and add your team members' names.

8. Stage and commit your changes.

9. Push your changes to your project on GitLab as follows:

    ```bash
    $ git push
    ```

    NOTE: this will fail! What error did you get?

10. Search for what is a protected branch in GitLab?


11. Figure out how to remove protection from the master branch on GitLab, and do it.

12. Retry pushing your changes.

13. Make, stage, commit, and push another change.



---
Copyright © 2019 Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.

# Copyright and Licensing - Activity

Role | Name | Description
-- | -- | --
Manager | | Manage time. Ensure all voices are heard. Ensure roles are respected. Help answer questions.
Recorder | | Record team's answers. Help answer questions. Sits in the middle.
Reporter | | Speaks for the teams during discussion.

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.


## Model

Watch the video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016.

1. Video with transcript: [https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing](https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing)
2. Video on Youtube (can speed up) [https://youtu.be/9kGrKBOytYM](https://youtu.be/9kGrKBOytYM)


## Questions

1. You have a new idea for a program, can you copyright that idea?
2. You have written a new program based on that idea, is that software copyright-able?
3. You have written a new program, what do you have to do to copyright that program.
4. Refer to [http://www.bitlaw.com/copyright/scope.html](http://www.bitlaw.com/copyright/scope.html), if you own the copyrights to something, what can you do that no one else can do?
5. You find the code for a program published on a website. Without knowing anything else about it, can you...
    1. View it?
    2. Run it?
    3. Post it on your site?
    4. Print it and post it on the refrigerator at work?
    5. Modify it and post the new version to your website?
    6. Save multiple copies on your hard drive?
    7. Make the application available and usable through a web interface that you write?
    8. If you want to do something that would violate copyright, what must you do?
6. What is a license? I.e., what is the purpose of a license?
7. When you license your program do you give up your copyright?
8. As the copyright holder, once you license your program, could you later license it under a different license?
9. If you relicense your program, can someone still use your program under the previous license?
10. Try to identify the license for the following projects:
    1. [https://github.com/openmrs/openmrs-core](https://github.com/openmrs/openmrs-core)
    2. [https://github.com/apache/incubator-fineract](https://github.com/apache/incubator-fineract)
    3. [https://github.com/regulately/regulately-back-end](https://github.com/regulately/regulately-back-end)
11. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannots” and the “musts” for each.
12. Can you sell open-source licensed software?
13. What's the difference between a permissive open-source license and a non-permissive ("viral") open-source license?

## Copyright and License of this work

The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.

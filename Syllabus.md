<small>Version 2.0 - Tue Jan 14 17:44:24 UTC 2020</small>

## CS 220 - Software Development

Participants will learn modern tools and practices to design and develop large
systems in teams such as integrated development environments, build systems,
testing, version control, and issue Tracking.

4 cr.

Requisites: complete CS-102 or IT-102 or CS-171


## Instructor Information

- Stoney Jackson, PhD
- Email: [Stoney.Jackson@wne.edu](mailto:Stoney.Jackson@wne.edu)
- Office: Herman 207b
- Office Hours:
  - Monday and Wednesday: 10:00 AM - Noon
  - Tuesday and Thursday: 11:00 AM - Noon
- Appointments: [https://stoneyjackson.youcanbook.me/](https://stoneyjackson.youcanbook.me/)


## Non-Free Required Materials

- Robert Martin. Clean Code: A Handbook of Agile Software Craftsmanship.
  1<sup>st</sup> ed. Prentice Hall. 2008. ISBN-13: 978-0132350884. ~$35

- ACM Student Membership. ~$19. One of the many benefits is that it gives you
  access to O'Reilly Online, which has numerous texts and videos on different
  topics. I may ask you to watch videos from this service or read selected
  chapters from texts, etc. In fact there is a great video series from by
  Robert Martin related to Clean Code.


## Recommended Non-Free Materials

- A Laptop with a supported version of Windows, MacOS, or Linux (but not
  Chromebook). You do have access to computer labs that should have the
  software we need in this class. But a Laptop would likely make your life
  easier.


## Evaluation and Grading

- 40% Exams (about 3)
- 40% Homework (about 6)
- 20% Attendance and Participation


### Exams

Exams assess your knowledge of concepts, terminology, and their correct
application. Each exam primarily covers the material since the last exam.


### Homework

Individually, you will complete assignments. These will be used to deepen your
knowledge and assess your ability to apply your knowledge.


### Grading Scale

The following scale is used to map overall scores to letter grades. The top is
the minimum overall percentage needed to earn the letter grade below. Overall
scores are rounded to the nearest percent before converting to a letter grade.

0 |	60 | 67 | 70 | 73 | 77 | 80 | 83 | 87 | 90 | 93
-- | -- | -- | -- | -- | -- | -- | -- | -- | -- | --
F |	D | D+ | C- | C | C+ | B- | B | B+ | A- | A


## Tentative Schedule

See [README.md](README.md)

## Standard Policies


### Change Clause

Changes may be made to the syllabus or these policies. Changes are announced in
class and a revised document will be posted to Kodiak. Use the timestamp in the
upper right corner to determine which version you have.


### Learning Disabilities

If you have a documented learning disability that requires special
accommodation, please call the Learning Disability Services Office at
413-782-1257.

**Note: **One or more of your peers may be allowed to record audio from class
session as part of an accommodation for personal use only. Therefore, be aware
that your participation in class may be recorded.


### Academic Honesty

Participating in cheating on an exam or an assignment will be reported to the
chair who will determine an appropriate penalty.


### Attendance and Participation

You will be working in teams of 2-3. The purpose of these activities is to
learn material and develop process skills. Your score for this work will be
based on your participation and contribution to your team. To be successful in
your team you will need to be prepared, be present, contribute, and
communicate.

Missing class or being late is very detrimental to your team and the overall
learning experience. If you know you cannot make a class, make sure your team
knows, and that you provide them with any materials you are responsible for
before class.

Each day, you will receive one of the following scores for attendance and
participation (A&P).

- P:	100%	Present when roll was taken and no other score applies
- L:	75%	Late: after roll has been taken but before 15 minutes after class started
- IP: 50%	Insufficient Participation: e.g., not on task, not cooperating with your team, miss 15 or more minutes of class, etc.
- A:	0%	Absent: did not make your presence known to the instructor
- AA: --	Authorized Absence: does not count toward A&P

Even when you are absent, you are responsible all work due that day,
exams/quizzes/etc. proctored that day, and all materials presented or given out
that day.

If you are absent (A) on the day of an exam, quiz, or in-class assignment, you
will receive a 0 for that element. See _extenuating exceptions_ for exceptions.


### Late Work

A 5% penalty per day late will be applied for the first 2 days late. On the
3<sup>rd </sup>day late, late work will receive no more than 55%. It will be
checked for authenticity and completeness, and will not receive critical
feedback. Homework more than 1 week late will receive a 0.  Work not submitted
correctly is considered not submitted. Late work will not be accepted after the
final exam (or the last day of class if there is no final), and will receive a
0. For exceptions, see Extenuating Circumstances.  Unless otherwise specifide,
work is due by 9 AM ET, on the designated due date.


### Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies,
you may request special considerations for your circumstance. Make a request as
follows:

*   Complete the Extenuating Circumstance "quiz" on Kodiak.
*   Submit hard copies of evidence to your instructor.

Appeals must be submitted as soon as possible once you are aware of the
extenuating circumstances. Your instructor will determine the acceptability of
an appeal, along with the specific accommodations that will be made.

_If you have any problems with the above instructions, please discuss your
situation with your instructor._


### Submission Guidelines

Your score for an assignment may be negatively impacted if you do not follow
these guidelines. For small assignments, not following these guidelines may
result in a less than passing score. Violating some of these may result in a 0
or a late penalty: for example, submitting work to the wrong dropbox or wrong
Kodiak classroom.

*   **Do submit work to the correct Kodiak DropBox** in the correct Kodiak classroom.
*   **Do test your submission** after submitting by downloading your submission from Kodiak and viewing its contents to ensure that what you think you submitted is actually what you submitted.
*   **Do place proper headers on each file you edit**. All files you edit must have a header at the top with the following information:
    *   Full name
    *   Date
    *   Class
    *   Assignment
    *   File
*   **Do** make sure that the proper headers do not break your code. **Put proper headers in comments**, etc.
*   **Do cite** your sources. Just as in English, you need to credit ideas and work that are not your own in anything you submit. If you do not cite a source, you are taking credit for and claiming ownership of what you submit. If you are not the owner, and you do not cite the source, then you are plagiarizing and in violation of academic honesty.
*   **Do format** your code correctly and consistently. Any standard format is acceptable. But you must be consistent. Be careful about mixing tabs and spaces, as your code may not appear the same when viewed in a different environment.
*   **Do zip your files** when submitting multiple files. A zip is not necessary if you are submitting a single file.
*   **Do not submit extra files**. Only files that you modified and files necessary to run your code should be submitted.
*   **Do submit all files needed to run your code.**
*   **Do rezip and resubmit** all files if you need to resubmit a single file for an assignment.
*   **Do organize files** appropriately in directories and name files appropriately.
*   Do organize and staple hardcopies **<span style="text-decoration:underline;">if</span>** hardcopies are required.


<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
# Materials for CS-220 Software Development 


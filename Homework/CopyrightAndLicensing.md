*Version CS-220-Spring-2020-1.0*
# Homework for Copyright and Licensing


## Overview

Explore different types of licenses frequently used by open source projects.


## Prerequisites

* None


## Objectives

* Identify a project’s license
* Identify constraints license imposes on your contributions
* Articulate whether or not you are comfortable contributing to a project with a given license


## Process skills

* Critical Thinking
* Communication


## Exercise Technical Skills

* Markdown
* Git (clone, add, commit, and push)
* GitLab (create a project in a given group)


## Background

A quick YouTube search will yield a number of good videos on copyright and licensing. We recommend watching one or two on each topic: copyright and licensing. 

Here are a few of our favorites. The first two are particularly recommended:

  - Copyright Clearance Center. “Copyright Basics”. 2010.
    [https://youtu.be/Uiq42O6rhW4](https://youtu.be/Uiq42O6rhW4) . This video explains copyrights from
    a business perspective.
  - PyCon 2016: Felix Crux, "What You Need To Know About Free & Open Source Software Licensing". [https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing](https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing). His site includes the video, slides, and transcript of his excellent talk, plus a bibliography about open source licenses.
  - Intel Software. “Open Source Basics”. 2014.
    [https://youtu.be/Tyd0FO0tko8](https://youtu.be/Tyd0FO0tko8) . This video explains how open source
    software is enabled through licensing.
  - Watch Now, UK. “Creative Commons & Copyright Info”. 2012.
    [https://youtu.be/8YkbeycRa2A](https://youtu.be/8YkbeycRa2A) This video explains the purpose of
    licenses from a content producer’s view who wants to let other use
    their work without having to ask permission. It specifically talks
    about the Creative Commons licenses.
  
## Directions

1.  Identify and record the license for the following projects. (You do not need to look beyond the GitHub repository for each project):
    - [https://github.com/openmrs/openmrs-core](https://github.com/openmrs/openmrs-core)

    - [https://github.com/apache/incubator-fineract](https://github.com/apache/incubator-fineract)

    - [https://github.com/regulately/regulately-back-end](https://github.com/regulately/regulately-back-end)

    - [https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market](https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market)

2.  Go to [https://tldrlegal.com/](https://tldrlegal.com/). Look up each of the above licenses.
    Identify and record the “cans” the “cannots” and the “musts” for each.

3.  For each license, state whether you would (or would not) be
    comfortable contributing code to that project and why (or why not).


## Deliverables

Your instructor will supply you with a personal private GitLab group that you can submit your work to.

1. Create and initialize a project named `CopyrightAndLicensing` in your personal private GitLab group for this course.

2. Clone this project to your local machine.

    ```bash
    $ git clone CLONE-URL
    ```

3. Copy this markdown file into the root of your project.

4. Edit your copy of this file and answer the questions. I recommend using Atom, Visual Studio Code, or Notepad++ to edit this file. Atom has a previewer for Markdown (it is an extension you must install). Visual Studio Code comes built in with a previewer for Markdown. Notepad++ does not have a previewer (as far as I know).

5. When you are done, submit your homework by staging, committing, and pushing your changes to your project on GitLab.

    Be sure to run these commands from within the root of your project (you may need to do something like `cd CopyrightAndLicensing`).

    ```bash
    $ git add .
    $ git commit -m "Finished"
    $ git push
    ```

You may submit partial work as you go by regularly saving, staging, committing, and pushing your changes. This will also alow you to clone your work to another workstation and continue working from where you left off.

> ***IMPORTANT!*** Do not wait until the last minute to submit your work as you will undoubtedly encounter technical challenges along the way.

---
#### &copy; 2019 Stoney Jackson and Karl R. Wurst

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
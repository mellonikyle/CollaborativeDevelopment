# CS 220


## Quick Reference Links

* [Syllabus](Syllabus.md)
* Class group: https://gitlab.com/wne-csit/cs-220/spring-2020/class
* Class agenda/board: https://gitlab.com/groups/wne-csit/cs-220/spring-2020/class/-/boards


## Tentative Schedule

Week | Date | Day | Topic
:--: | :--: | :--: | :--:
1 | 1-13 | M | Setup
1 | 1-14 | T | Copyright and Licensing
1 | 1-15 | W | Agile
1 | 1-16 | R | Scrum
2 | 1-20 | M | MLK (no class)
2 | 1-21 | T | File System Navigation and Git Setup
2 | 1-22 | W | Saving with Git
2 | 1-23 | R | Undoing with Git
3 | 1-27 | M | Create a project on GitLab, cloning with Git, and pushing with Git
3 | 1-28 | T | Launch first homework and help debug git/gitlab connection issue
3 | 1-29 | W | .gitignore, commit messages, and .gitattributes
3 | 1-30 | R | Branching, Merging
4 | 2-3 | M | Conflicts
4 | 2-4 | T | **Homework Copyright And Licensing Due** and Conflicts
4 | 2-5 | W | Conflicts
4 | 2-6 | R |

